import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { APP_URL } from '../app.constant';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  httpClient = inject(HttpClient);
  constructor() { }

  addTask(task:string){
    return this.httpClient.post(`${APP_URL}/save/task`,{
      name:task
    })
  }
  getAllTasks(){
     return this.httpClient.get(`${APP_URL}/get/task`);
  }
  updateTask(task:any){
    return this.httpClient.put(`${APP_URL}/update/task`, task);
  }
}
